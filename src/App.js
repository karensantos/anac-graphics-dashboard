import React, { Component } from 'react';
import { Container, Row, Col, Card, Nav} from 'react-bootstrap';
import './App.css';
import Chart from './components/Chart';
import api from './service/api';

class App extends Component {
  constructor(){
    super();
    this.state = {
      chartDataVoos:{
        labels: [],
        datasets:[]
      },
      year: 0,
      qtdTotal: -1,
      qtdSemAtraso: -1,
      qtdComAtraso: -1,
      qtdCancelado: -1,
      backgroundColor: [
        'rgba(255, 159, 64, 0.9)',
        'rgba(54, 162, 235, 0.9)',
        'rgba(255, 206, 86, 0.9)',
        'rgba(75, 192, 192, 0.9)',
        'rgba(153, 102, 255, 0.9)',
      ],
      erro: false
    }
  }

  componentWillMount(){
    this.getYearData(2018);
  }

  async getYearData(year){
    const response = await api.get(`/getAno/${year}`);

    const process = response.data;
    console.log(process)
    if(process.status === 'sucesso'){
      var mesProcess = process.dados.dadosPorCadaMes.map(element => {
        console.log(element)
        var mes = {
          label: element.Mes,
          data:[
            element.Total,
            element.Total_Atrasados,
            element.Total_Cancelados,
            element.Total_No_Hoario,
          ],
          backgroundColor: this.state.backgroundColor
        }
        return mes;
      });

      this.setState({
        chartDataVoos:{
          labels: ['Atrasados','Cancelados', 'No Horário' ],
          datasets: mesProcess
        },

        year,
        qtdTotal: process.dados.qtdVoosProgramado,
        qtdSemAtraso: process.dados.qtdSemAtraso,
        qtdComAtraso: process.dados.qtdComAtraso,
        qtdCancelado: process.dados.qtdCancelado,
        erro: false
      })
    }else{
      this.setState({
        chartDataVoos:{
          labels: ['Atrasados','Cancelados', 'No Horário' ],
          erro: true,
          datasets: []
        },
      })
    }
  }

  render(){
    const handleSelect = (eventKey) => {
      console.log('rolou mudança');
      this.getYearData(eventKey);

      console.log(this.state.chartDataVoos.datasets.length)
      if(this.state.erro == true){
        alert("dados não localizados na base até o momento...");
      }

    }

    return (
      <div className="App">
        <Container>
          <Row className="mb-5 mt-2">
            <Col lg={12} className="header text-center">
                  <h1>ANAC</h1>
                  <h2 >Relatorios de Voos {this.state.year ? (`- ${this.state.year}`) : null }</h2>
            </Col>
          </Row>
        </Container>     
        <hr/>
        <Container>
          <Row>
            {/* link for year*/}
            <Col xs={12} sm={12} md={2} lg={2} className="col-years">
              <div className="nav-years p-3"> 
                  <Nav variant="pills" activeKey={this.state.year} className="flex-column" onSelect={handleSelect}>
                      <Nav.Link eventKey={2020}>2020</Nav.Link>
                      <Nav.Link eventKey={2019}>2019</Nav.Link>
                      <Nav.Link eventKey={2018}>2018</Nav.Link>
                      <Nav.Link eventKey={2017}>2017</Nav.Link>
                      <Nav.Link eventKey={2016}>2016</Nav.Link>
                      <Nav.Link eventKey={2015}>2015</Nav.Link>
                  </Nav>
              </div>
            </Col>

            {/* content area */}
            <Col xs={12} sm={12} md={10} lg={10}>

              <Row className="justify-content-md-center mb-5">
                {/* cards show details */}
                <Col xs={12} md={3} lg={3}>
                  <Card>
                      <Card.Body>
                        <Card.Title  className="d-flex">Total de Voos Programados </Card.Title>
                        <Card.Text>
                          <br/>
                          <i className="fa fa-calendar-alt fa-2x mr-4"></i>
                          {this.state.qtdTotal}
                        </Card.Text>                    
                      </Card.Body>
                    </Card>
                </Col>
    
                <Col xs={12} md={3} lg={3}>
                  <Card>
                      <Card.Body>
                        <Card.Title className="d-flex">Total de Voos Cancelados <i className="fa fa-ban fa-2x"></i></Card.Title>
                        <Card.Text>
                          {this.state.qtdCancelado}
                        </Card.Text>                    
                      </Card.Body>
                    </Card>
                </Col>

                <Col xs={12} md={3} lg={3}>
                  <Card>
                      <Card.Body>
                        <Card.Title className="d-flex">Total de Voos Realizados Com Atraso <i className="fa fa-history fa-2x mr-4"></i></Card.Title>
                        <Card.Text>
                          {this.state.qtdComAtraso}
                        </Card.Text>                    
                      </Card.Body>
                    </Card>
                </Col>

                <Col xs={12} md={3} lg={3}>
                  <Card>
                      <Card.Body>
                        <Card.Title className="d-flex">Total de Voos Realizados Pontualmente<i className="fa fa-plane fa-2x"></i></Card.Title>
                        <Card.Text>
                          {this.state.qtdSemAtraso}
                        </Card.Text>                    
                      </Card.Body>
                    </Card>
                </Col>
                {/* ./cards show details */}
              </Row>
            
              <Row>
                <Col xs={12} sm={12} md={12} lg={12}> 
                {this.state.erro == true ? null :
                  (<Chart chartDataVoos={this.state.chartDataVoos} year="" type="Bar"/>)
                }
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={12} md={6} lg={6}> 
                {this.state.erro == true ? null :
                  (<Chart chartDataVoos={this.state.chartDataVoos} year="" type="Doughnut"/>)
                }
                </Col>
                <Col xs={12} sm={12} md={6} lg={6}> 
                {this.state.erro == true ? null :
                  (<Chart chartDataVoos={this.state.chartDataVoos} year="" type="Pie"/>)
                }
                </Col>
              </Row>
          </Col>
          {/* content area */}
          </Row>
          {/* <Row className="mt-5">
              <hr/>
                <Col xs={12} sm={12} md={12} lg={12}>
                  <h2>Detalhamento mensal</h2>
                </Col>
          </Row>
          <Row className="mt-5">
                <Col xs={12} sm={12} md={6} lg={6}>
                </Col>
                <Col xs={12} sm={12} md={6} lg={6}> 
                  <h2>Janeiro</h2>
                  <Chart chartDataVoos={this.state.chartDataVoos} all={true} position={2} year="" type="Pie"/>
                </Col>
              </Row> */}
        </Container>
      </div>
    );
  }
}

export default App;
