import React, {Component} from 'react';
import { Doughnut,Line, Pie, Bar  } from 'react-chartjs-2';

class Chart extends Component {
    constructor(props){
        super(props);
        this.state = {
            chartDataVoos: this.props.chartDataVoos,
            type:props.type,
            all:false
        }
    }

    static defaultProps = {
        displayTitle: true,
        displayLegend: true,
        legendPosition: 'right',
        year: 'Ano',
    }

    render () {
        console.log(this.props.chartDataVoos)
        switch(this.props.type) {
            case 'Line':
                return(
                    <div className="chart"> 
                        <Line
                            data={this.props.chartDataVoos}
                            width={100}
                            height={30}
                            options={{
                                title: {
                                    display: this.props.display,
                                    text:'Relatório de voos ',
                                    fontSize: '25'
                                },
                                legend: {
                                    display: this.props.displayLegend,
                                    position: this.props.legendPosition,
                                }
                            }}
                        />
                    </div>
                )
            case 'Bar':
                return(
                    <div className="chart"> 
                        <Bar
                            data={this.props.chartDataVoos}
                            width={100}
                            height={30}
                            options={{
                                title: {
                                    display: this.props.display,
                                    text:'Relatório de voos ',
                                    fontSize: '25'
                                },
                                legend: {
                                    display: this.props.displayLegend,
                                    position: this.props.legendPosition,
                                }
                            }}
                        />
                    </div>
                )
            case 'Pie':
                return(
                    <div className="chart"> 
                        <Pie
                            data={this.props.chartDataVoos}
                            width={100}
                            height={30}
                            options={{
                                title: {
                                    display: this.props.display,
                                    text:'Relatório de voos ',
                                    fontSize: '25'
                                },
                                legend: {
                                    display: this.props.displayLegend,
                                    position: this.props.legendPosition,
                                }
                            }}
                        />
                    </div>
                )
            default:
                return(
                    <div className="chart"> 
                    <Doughnut
                        data={this.props.chartDataVoos}
                        width={100}
                        height={30}
                        options={{
                            title: {
                                display: this.props.display,
                                text:'Relatório de voos ',
                                fontSize: '25'
                            },
                            legend: {
                                display: this.props.displayLegend,
                                position: this.props.legendPosition,
                            }
                        }}
                    />
                    </div>
                )
        }

    }
}

export default Chart;